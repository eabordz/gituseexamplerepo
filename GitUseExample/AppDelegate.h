//
//  AppDelegate.h
//  GitUseExample
//
//  Created by Optimind on 11/28/13.
//  Copyright (c) 2013 Optimind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
